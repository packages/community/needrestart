# Maintainer: Mark Wagie <mark[at]manjaro[dot]org>
# Contributor: Helmut Stult

pkgname=needrestart
pkgver=3.8
pkgrel=1
pkgdesc="Restart daemons after kernel, library and CPU microcode updates."
arch=('any')
url="https://github.com/liske/needrestart"
license=('GPL-2.0-or-later')
depends=(
  'perl-libintl-perl'
  'perl-module-find'
  'perl-module-scandeps'
  'perl-proc-processtable'
  'perl-sort-naturally'
  'perl-term-readkey'
)
makedepends=('git')
optdepends=(
  'iucode-tool: for outdated Intel microcode detection'
  'libnotify: notify a user session via dbus'
)
backup=("etc/$pkgname/$pkgname.conf"
        "etc/$pkgname/notify.conf")
source=("git+https://github.com/liske/needrestart.git#tag=v$pkgver")
sha256sums=('a0cbb264784fb887f81a15971297e72189058c554fb699d110e3395dfd9ec213')

prepare() {
  cd "$pkgname"
  find . -type f -exec sed -i 's/sbin/bin/g' {} \;
}

build() {
  cd "$pkgname"
  unset PERL5LIB PERL_LOCAL_LIB_ROOT PERL_MB_OPT PERL_MM_OPT
  export PERL_MM_USE_DEFAULT=1 PERL_AUTOINSTALL=--skipdeps
  make
}

package() {
  cd "$pkgname"
  unset PERL5LIB PERL_LOCAL_LIB_ROOT PERL_MB_OPT PERL_MM_OPT
  make DESTDIR="$pkgdir" install

  # remove empty dirs; '!emptydirs' doesn't remove them
  rm -rf "$pkgdir/usr/lib/perl5/"
}
